FROM ubuntu:18.04
MAINTAINER David Broin <davidmbroin@gmail.com>

# Install core dependencies
RUN apt-get update && \
    apt-get -y install -y \
        gcc \
        clang \
        make \
        cmake \
        ruby \
        ruby-dev \
        libffi-dev \
        libboost-all-dev \
        git

#Follow the steps of https://github.com/cucumber/cucumber-cpp/blob/master/README.md
RUN git clone https://github.com/cucumber/cucumber-cpp.git
RUN gem install bundler
RUN cd cucumber-cpp && \
    bundle install && \
    git submodule init && \
    git submodule update && \
    cmake -E make_directory build && \
    cmake -E chdir build cmake -DCMAKE_INSTALL_PREFIX=${prefix} .. && \
    cmake --build build && \
    cmake --build build --target test && \
    cmake --build build --target install && \
    cmake --build build --target features